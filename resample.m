function [newSamples, newWeight] = resample(samples, weight)

% do some stuff here
N = length(weight);
newWeight = zeros(N,1);
newWeight(:) = 1/N;
sw = sum(weight);
start = rand()*(sw/N);
current = start;
step = sw/N;
newSamples = zeros(3,N);
for i = 1:N
    pos = 1;
    counter = weight(1);
    while(1)
        if counter > current
            break;
        else
            pos = pos +1;
            counter = counter + weight(pos);
        end
    end
    newSamples(:,i) = samples(:,pos);
    current = current+step;
end
end