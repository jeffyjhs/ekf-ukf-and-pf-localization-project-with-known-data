function [mu, Sigma, predMu, predSigma, zhat] = ukfUpdate( ...
    mu, Sigma, u, deltaT, M, z, Q, markerId)

% NOTE: The header is not set in stone.  You may change it if you like.
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;

% --------------------------------------------
% Setup UKF
% --------------------------------------------
zhat = 0;
predMu = zeros(3,1);
predSigma = zeros(3);
% UKF params
% theta = mu(3);
a = 1;
beta = 2;
k = 0;
lamda = a^2*(7+k)-7;
% Augmented state
augMu = [mu;zeros(3,1);0];
augSigma = [Sigma,zeros(3),zeros(3,1);
            zeros(3),M,zeros(3,1);
            zeros(1,3),zeros(1,3),Q];
% Sigma points
Xt_1 = [augMu, repmat(augMu,1,7)+sqrt(7+lamda)*sqrtm(augSigma), repmat(augMu,1,7)-sqrt(7+lamda)*sqrtm(augSigma)];
% Weights
wm = [(lamda/(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda))];
wc = [(lamda/(7+lamda))+1-a^2+beta, 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda)), 1/(2*(7+lamda))];
% --------------------------------------------
% Prediction step
% --------------------------------------------
XBar = zeros(3,15);
for i = 1:15
    uComb = u + Xt_1(4:6,i);
    theta = Xt_1(3,i);
    XBar(:,i) = Xt_1(1:3,i)+[uComb(2)*cos(theta+uComb(1));
                          uComb(2)*sin(theta+uComb(1));
                          uComb(1)+uComb(3)];
end
% UKF prediction of mean and covariance
for i = 1:15
    predMu = predMu +  wm(i)*XBar(:,i);
end
predMu(3) = corAng(predMu(3));
for i = 1:15
    delta = XBar(:,i)-predMu;
    delta(3) = corAng(delta(3));
    predSigma = predSigma + wc(i)*(delta)*(delta)';
end
%--------------------------------------------------------------
% Correction step
%--------------------------------------------------------------
zbar = zeros(15,1);
for i = 1:15
    zbar(i) = atan2(landmark_y-XBar(2,i),landmark_x-XBar(1,i))-XBar(3,i)+Xt_1(7,i);
end
for i = 1:15
    zhat = zhat + wm(i)*zbar(i);
end
zhat = corAng(zhat);
S = 0;
SigmaXZ = 0;
for i = 1:15
    S = S+wc(i)*(corAng(zbar(i)-zhat)^2);
    delta = XBar(:,i)-predMu;
    delta(3) = corAng(delta(3));
    SigmaXZ = SigmaXZ +wc(i)*(delta)*(corAng(zbar(i)-zhat));
end


K = SigmaXZ/S;
% UKF correction of mean and covariance
mu = predMu + K*(z-zhat);
Sigma = predSigma - K*S*K';
% disp(augMu);
% disp(augSigma);
% disp(Xt_1);
end

function ang = corAng(ang)
if ang <= -1*pi
    while(1)
        ang = ang + 2*pi;
        if ang >= -1*pi;
            break;
        end
    end
elseif ang >pi
    while(1)
        ang = ang - 2*pi;
        if ang < pi;
            break;
        end
    end
end
end