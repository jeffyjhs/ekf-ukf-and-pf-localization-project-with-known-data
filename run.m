function varargout = run(stepsOrData,method ,pauseLen, makeVideo)
% RUN  PS2 Feature-Based Localization Simulator
%   RUN(ARG)
%   RUN(ARG, PAUSLEN, MAKEVIDEO)
%      ARG - is either the number of time steps, (e.g. 100 is a complete
%            circuit) or a data array from a previous run.
%      PAUSELEN - set to `inf`, to manually pause, o/w # of seconds to wait
%                 (e.g., 0.3 is the default)
%      MAKEVIDEO - boolean specifying whether to record a video or not
%
%   DATA = RUN(ARG,PAUSELEN)
%      DATA - is an optional output and contains the data array generated
%             and/or used during the simulation.

%   (c) 2009-2015
%   Ryan M. Eustice
%   University of Michigan
%   eustice@umich.edu

if ~exist('pauseLen','var') || isempty(pauseLen)
    pauseLen = 0.3; % seconds
end
if ~exist('makeVideo','var') || isempty(makeVideo)
    makeVideo = false;
end

%--------------------------------------------------------------
% Graphics
%--------------------------------------------------------------

NOISEFREE_PATH_COL = 'green';
ACTUAL_PATH_COL = 'blue';

NOISEFREE_BEARING_COLOR = 'cyan';
OBSERVED_BEARING_COLOR = 'red';

GLOBAL_FIGURE = 1;

if makeVideo
    try
        votype = 'avifile';
        vo = avifile('video.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);
    end
end

%--------------------------------------------------------------
% Initializations
%--------------------------------------------------------------

initialStateMean = [rand()*500 rand*300 -1*pi+rand*2*pi]';

% Motion noise (in odometry space, see Table 5.5, p.134 in book).
 alphas = [0.05 0.001 0.05 0.01].^2; % variance of noise proportional to alphas
%  alphas = [0 0 0 0].^2;
% Standard deviation of Gaussian sensor noise (independent of distance) 
 beta = deg2rad(20);
%  beta = deg2rad(10);


% Step size between filter updates, can be less than 1.
deltaT=0.1;

persistent data numSteps;
if isempty(stepsOrData) % use dataset from last time
    if isempty(data)
        numSteps = 100;
        data = generateScript(initialStateMean, numSteps, alphas, beta, deltaT);
    end
elseif isscalar(stepsOrData)
    % Generate a dataset of motion and sensor info consistent with
    % noise models.
    numSteps = stepsOrData;
    data = generateScript(initialStateMean, numSteps, alphas, beta, deltaT);
else
    % use a user supplied dataset from a previous run
    data = stepsOrData;
    numSteps = size(data, 1);
    global FIELDINFO;
    FIELDINFO = getfieldinfo;
end


% TODO: provide proper initialization for your filters here
% You can set the initial mean and variance of the EKF to the true mean and
% some uncertainty.



% Call ekfUpdate, ukfUpdate and pfUpdate in every iteration of this loop.
% You might consider putting in a switch yard so you can select which
% algorithm does the update
results = [];

switch method
    case 'EKF'
        mu = [180 50 0]';
        Sigma = eye(3)*100000;
        err = zeros(3,numSteps);
        stdD = zeros(3,numSteps);
    case 'UKF'
        mu = [180 50 0]';
        Sigma = eye(3)*100000;
        err = zeros(3,numSteps);
        stdD = zeros(3,numSteps);
    case 'PF'
        N = 100;
        samples = [-200+700*rand(1,N-1),initialStateMean(1);
                   -200+500*rand(1,N-1),initialStateMean(2);
                   -1*pi+2*pi*rand(1,N-1),initialStateMean(3)];
        weight = zeros(N,1);
        weight(:) = (1/N);
        err = zeros(3,numSteps);
        stdD = zeros(3,numSteps);
end

for t = 1:numSteps

    %=================================================
    % data available to your filter at this time step
    %=================================================
    motionCommand = data(t,3:5)'; % [drot1, dtrans, drot2]' noisefree control command
    observation = data(t,1:2)';   % [bearing, landmark_id]' noisy observation

    %=================================================
    % data *not* available to your filter, i.e., known
    % only by the simulator, useful for making error plots
    %=================================================
    % actual position (i.e., ground truth) 
    x = data(t,8);
    y = data(t,9);
    theta = data(t,10);

    % noisefree observation
    noisefreeBearing = data(t, 6);

    %=================================================
    % graphics
    %=================================================
    figure(GLOBAL_FIGURE); clf; hold on; plotfield(observation(2));

    % draw actual path and path that would result if there was no noise in
    % executing the motion command
    plot([initialStateMean(1) data(1,8)], [initialStateMean(2) data(1,9)], 'Color', ACTUAL_PATH_COL);
    plot([initialStateMean(1) data(1,11)], [initialStateMean(2) data(1,12)], 'Color', NOISEFREE_PATH_COL);

    % draw actual path (i.e., ground truth)
    plot(data(1:t,8), data(1:t,9), 'Color', ACTUAL_PATH_COL);
    plotrobot( x, y, theta, 'black', 1, 'cyan');

    % draw noise free motion command path
    plot(data(1:t,11), data(1:t,12), 'Color', NOISEFREE_PATH_COL);
    plot(data(t,11), data(t,12), '*', 'Color', NOISEFREE_PATH_COL);

    % indicate observed angle relative to actual position
    plot([x x+cos(theta+observation(1))*100], [y y+sin(theta+observation(1))*100], 'Color', OBSERVED_BEARING_COLOR);

    % indicate ideal noise-free angle relative to actual position
    plot([x x+cos(theta+noisefreeBearing)*100], [y y+sin(theta+noisefreeBearing)*100], 'Color', NOISEFREE_BEARING_COLOR);

    %=================================================
    %TODO: update your filter here based upon the
    %      motionCommand and observation
    %=================================================
    switch method
        case 'EKF'
            M = diag([motionCommand(1)^2*alphas(1)+motionCommand(2)^2*alphas(2),motionCommand(2)^2*alphas(3)+motionCommand(1)^2*alphas(4)+motionCommand(3)^2*alphas(4),motionCommand(3)^2*alphas(1)+motionCommand(2)^2*alphas(2)]);
%             M = M*10;
            z = observation(1);
            markerId = observation(2);
%             Q = beta^2;
            Q = beta^2/10;
            [mu, Sigma, predMu, predSigma, zhat, G, H, K ] = ekfUpdate(mu, Sigma, motionCommand, deltaT, M, z, Q, markerId);
            err(:,t) = mu-[x;y;theta];
            err(3,t) = minimizedAngle(err(3,t));
            stdD(:,t) = [sqrt(Sigma(1,1)), sqrt(Sigma(2,2)), sqrt(Sigma(3,3))];
        case 'UKF'
            M = diag([motionCommand(1)^2*alphas(1)+motionCommand(2)^2*alphas(2),motionCommand(2)^2*alphas(3)+motionCommand(1)^2*alphas(4)+motionCommand(3)^2*alphas(4),motionCommand(3)^2*alphas(1)+motionCommand(2)^2*alphas(2)]);
            z = observation(1);
            markerId = observation(2);
%             Q = beta^2;
            Q = beta^2/10;
            [mu, Sigma, predMu, predSigma, zhat] = ukfUpdate(mu, Sigma, motionCommand, deltaT, M, z, Q, markerId);
            err(:,t) = mu-[x;y;theta];
            err(3,t) = minimizedAngle(err(3,t));
            stdD(:,t) = [sqrt(Sigma(1,1)), sqrt(Sigma(2,2)), sqrt(Sigma(3,3))];
        case 'PF'
%             M = diag([motionCommand(1)^2*alphas(1)+motionCommand(2)^2*alphas(2),motionCommand(2)^2*alphas(3)+motionCommand(1)^2*alphas(4)+motionCommand(3)^2*alphas(4),motionCommand(3)^2*alphas(1)+motionCommand(2)^2*alphas(2)]);
            z = observation(1);
            markerId = observation(2);
%             Q = beta;
            Q = beta/3;
            [samples, weight, mu, Sigma, predMu, predSigma, zHat] = pfUpdate(samples, weight, N, motionCommand, deltaT, alphas, z, Q, markerId);
            err(:,t) = mu-[x;y;theta];
            err(3,t) = minimizedAngle(err(3,t));
            stdD(:,t) = [sqrt(Sigma(1,1)), sqrt(Sigma(2,2)), sqrt(Sigma(3,3))];
    end

    %=================================================
    %TODO: plot and evaluate filter results here
    %=================================================
     
    switch method
        case 'EKF'
            plotmarker(mu,'red');
            plotcov2d(mu(1),mu(2),Sigma,'blue',false,'gray',0,3);
            plotmarker(predMu,'black');
            plotcov2d(predMu(1),predMu(2),predSigma,'green',false,'gray',0,3);
            title('EKF');
            xlabel('x location');
            ylabel('y location');
            txt = ['t = ',num2str(t*deltaT) ];
            text(x,y-50,txt );
        case 'UKF'
            plotmarker(mu,'red');
            plotcov2d(mu(1),mu(2),Sigma,'cyan',false,'gray',0,3);
            plotmarker(predMu,'black');
            plotcov2d(predMu(1),predMu(2),predSigma,'yellow',false,'gray',0,3);
            title('UKF');
            xlabel('x location');
            ylabel('y location');
            txt = ['t = ',num2str(t*deltaT) ];
            text(x,y-50,txt );
        case 'PF'
            plotmarker(mu,'red');
            plotcov2d(mu(1),mu(2),Sigma,'cyan',false,'gray',0,3);
            plotmarker(predMu,'black');
            plotcov2d(predMu(1),predMu(2),predSigma,'yellow',false,'gray',0,3);
            plotSamples(samples);
            title('PF');
            xlabel('x location');
            ylabel('y location');
            txt = ['t = ',num2str(t*deltaT) ];
            text(x,y-50,txt );
    end     

    drawnow;
    if pauseLen == inf
        pause;
    elseif pauseLen > 0
        pause(pauseLen);
    end

    if makeVideo
        F = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
          otherwise
            error('unrecognized votype');
        end
    end
end
time = 1:numSteps;
time = time *deltaT;
figure(2);
subplot(3,1,1);
plot(time,err(1,:),'r',time,3*stdD(1,:),'b',time,-3*stdD(1,:),'b');
hold on ;
title('X error and 3 sigma bound');
xlabel('Time (sec)');
ylabel('cm');
hold off;
subplot(3,1,2);
plot(time,err(2,:),'r',time,3*stdD(2,:),'b',time,-3*stdD(2,:),'b');
hold on ;
title('Y error and 3 sigma bound');
xlabel('Time (sec)');
ylabel('cm');
hold off;
subplot(3,1,3);
plot(time,err(3,:),'r',time,3*stdD(3,:),'b',time,-3*stdD(3,:),'b');
hold on ;
title('\theta error and 3 sigma bound');
ylabel('rad');
xlabel('Time (sec)');
hold off;

if nargout >= 1
    varargout{1} = data;
end
if nargout >= 2
    varargout{2} = results;
end

if makeVideo
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end
