function [mu, Sigma, predMu, predSigma, zhat, G, H, K ] = ekfUpdate( ...
    mu, Sigma, u, deltaT, M, z, Q, markerId)

% NOTE: The header is not set in stone.  You may change it if you like.
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;

% --------------------------------------------
% Prediction step
% --------------------------------------------
theta = mu(3);
G = [1, 0, -1*u(2)*sin(theta+u(1));
     0, 1, u(2)*cos(theta+u(1));
     0, 0, 1];
V = [-1*u(2)*sin(theta+u(1)), cos(theta+u(1)), 0;
     u(2)*cos(theta+u(1)), sin(theta+u(1)), 0 ;
     1, 0, 1];


% EKF prediction of mean and covariance
predMu = mu + [u(2)*cos(theta+u(1));
                u(2)*sin(theta+u(1));
                u(1)+u(3)];
if predMu(3) <= -1*pi
    while(1)
        predMu(3) = predMu(3) + 2*pi;
        if predMu(3) >= -1*pi;
            break;
        end
    end
elseif predMu(3) >pi
    while(1)
        predMu(3) = predMu(3) - 2*pi;
        if predMu(3) < pi;
            break;
        end
    end
end
predSigma = G*Sigma*G'+V*M*V';

%--------------------------------------------------------------
% Correction step
%--------------------------------------------------------------

% Compute expected observation and Jacobian
zhat = atan2(landmark_y-predMu(2),landmark_x-predMu(1))-predMu(3);
if zhat <= -1*pi
    while(1)
        zhat = zhat + 2*pi;
        if zhat >= -1*pi;
            break;
        end
    end
elseif zhat >pi
    while(1)
        zhat = zhat - 2*pi;
        if zhat < pi;
            break;
        end
    end
end
d = (landmark_x-predMu(1))^2+(landmark_y-predMu(2))^2;
H = [(landmark_y-predMu(2))/d, (-1*landmark_x+predMu(1))/d, -1];
% Innovation / residual covariance
S = H*predSigma*H'+Q;
% Kalman gain
K = predSigma*H'/S;
% Correction
mu = predMu+K*(z-zhat);
Sigma = (eye(3)-K*H)*predSigma;
if mu(3) <= -1*pi
    while(1)
        mu(3) = mu(3) + 2*pi;
        if mu(3) >= -1*pi;
            break;
        end
    end
elseif mu(3) >pi
    while(1)
        mu(3) = mu(3) - 2*pi;
        if mu(3) < pi;
            break;
        end
    end
end
end