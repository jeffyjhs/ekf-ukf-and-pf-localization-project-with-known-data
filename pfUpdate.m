function [samples, weight, mu, Sigma, predMu, predSigma, zHat] = pfUpdate( ...
    samples, weight, numSamples, u, deltaT, alphas, z, Q, markerId)

% NOTE: The header is not set in stone.  You may change it if you like
global FIELDINFO;

stateDim=3;
motionDim=3;
observationDim=2;


% ----------------------------------------------------------------
% Prediction step
% ----------------------------------------------------------------
N  = length(weight);
motionSamples = zeros(3,N);
newWeight = zeros(N,1);
% some stuff goes here
for i = 1:N
    motionSamples(:,i) = sampleOdometry(u,samples(:,i),alphas);
end
% Compute mean and variance of estimate. Not really needed for inference.
[predMu, predSigma] = meanAndVariance(motionSamples, N);


% ----------------------------------------------------------------
% Correction step
% ----------------------------------------------------------------
for i = 1:N
    ang=observation(motionSamples(:,i),markerId);
    zHat = ang(1);
    newWeight(i) = normpdf(z(1),zHat,Q);
end
[samples, weight] = resample(motionSamples, newWeight);
% more stuff goes here
[mu, Sigma] = meanAndVariance(samples, N);
end

